colorscheme onehalfdark
let g:airline_theme='onehalflight'
set t_Co=256
set cursorline
set nocompatible
syntax on
filetype plugin indent on
set mouse=a
set number
set laststatus=2 ignorecase hlsearch incsearch
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
autocmd BufNewFile,BufRead *.ezt set filetype=html
set smartindent
if exists('+termguicolors')
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    set termguicolors
endif
let g:lightline = {
            \ 'active':{
            \  'left': [ ['mode', 'paste' ],
            \            ['gitbranch', 'readonly','filename', 'modified' ] ]
            \   },
            \   'component_function':{
            \       'gitbranch': 'gitbranch#name'
            \   },
            \}
call plug#begin('~/.vim/plugged')
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'sonph/onehalf',{'rtp':'vim/'}

